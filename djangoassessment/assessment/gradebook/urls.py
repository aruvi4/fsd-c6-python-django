from django.urls import path
from .views import get_student_details, get_grades, add_score

app_name = 'gradebook'

urlpatterns = [
    path('student/<int:student_id>', get_student_details, name='student_details'),
    path('student/<int:student_id>/grades', get_grades, name='grades'),
    path('student/<int:student_id>/grades/add', add_score, name='add_score')
]