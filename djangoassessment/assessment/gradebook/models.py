from django.db import models

'''
Each Student has multiple subjects each of which has a corresponding score.
This has been modelled using a Student table and a Subject table that has a foreign key
relationship to the Student table's primary key (id).
This means each Student object has a subject_set which is a set of Subject objects that contain scores.
'''

class Student (models.Model):
    name = models.CharField(max_length=256)
    roll_number = models.IntegerField(null=False)
    institution = models.CharField(max_length = 256)

class Subject (models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    subject_name = models.CharField(max_length=100)
    score = models.IntegerField(null=False)