from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Student, Subject
def get_student_details(request, student_id):
    student = get_object_or_404(Student, pk=student_id)
    return render(request, 'gradebook/student_details.html', {'student': student})

def get_grades(request, student_id):
    student = get_object_or_404(Student, pk=student_id)
    #your code here
    return render(request, 'gradebook/grades.html', {'student': student})

def add_score(request, student_id):
    #your code here
    return HttpResponse("I don't do anything yet")
