import mysql.connector
from Employee import Employee
cnx = mysql.connector.connect(user='javauser', password='password',
                              host='127.0.0.1',
                              database='mydb')

cursor = cnx.cursor(named_tuple = True)

FETCH_EMPLOYEE_FROM_ID = "SELECT * FROM EMPLOYEE WHERE id = %s"

id = int(input("Enter the ID to search "))

cursor.execute(FETCH_EMPLOYEE_FROM_ID, (id,))

for item in cursor:
    e = Employee(item.id, item.name, item.salary)
    print(e)

cursor.close()
cnx.close()

