from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse
from django.views import generic
import requests

from .models import Employee, Attendance, Nonsense

def index(request):
    return render(request, 'shop/home.html')

def allemployees(request):
    context = {'all_employees': Employee.objects.all()}
    return render(request, 'shop/allemployees.html', context)

def employee_detail(request, employee_id):
    employee = get_object_or_404(Employee, pk=employee_id)
    attendance_records = employee.attendance_set.all().order_by('date')
    return render(request, 'shop/employee_detail.html', {'employee': employee, 'attendance_records': attendance_records})

class AttendancePageView (generic.DetailView):
    model = Employee
    template_name = 'shop/employee_attendance.html'


def show_attendance_page (request, employee_id):
    employee = get_object_or_404(Employee, pk=employee_id)
    return render(request, 'shop/employee_attendance.html', {'employee': employee})

def giveattendance(request, employee_id):
    employee = get_object_or_404(Employee, pk=employee_id)
    date = request.POST['attendance_date']
    is_present = request.POST['attendance'] == 'present'
    attendance = Attendance(employee=employee,date=date,is_present=is_present)
    attendance.save()
    return HttpResponseRedirect(reverse('shop:employee_detail', args=(employee_id,)))

class NonsenseView (generic.ListView):
    template_name = 'shop/message_book.html'
    #context_object_name = 'nonsenses'

    def get_queryset(self):
        return Nonsense.objects.all()

def show_restricted_page(request):
    #request.session['allowed_access'] might raise a KeyError
    user_name = request.session.get('name', 'aruvi')
    response = requests.get(f'http://api.agify.io/?name={user_name}')
    if response.status_code != 200:
        raise ValueError('some issue in fetching agify data')
    agify_data = response.json()
    print(agify_data['age'])
    allowed_access = agify_data['age'] != None and agify_data['age'] > 21
    return render(request, 'shop/restricted.html', {'allowed_access': allowed_access})

def toggle_access(request):
    request.session['name'] = request.POST['name']
    return HttpResponseRedirect(reverse('shop:show_restricted_page'))

def addnonsense(request):
    new_message = request.POST['newmessage']
    n = Nonsense(message=new_message)
    n.save()
    return HttpResponseRedirect(reverse('shop:display_nonsense'))