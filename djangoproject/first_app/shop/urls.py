from django.urls import path, include
from .views import index, allemployees,employee_detail, AttendancePageView, giveattendance, NonsenseView, addnonsense, show_restricted_page, toggle_access

app_name='shop'

urlpatterns = [
    path('', index, name = 'index'),
    path('employees/all', allemployees, name = 'allemployees'),
    path('employee/<int:employee_id>', employee_detail, name = 'employee_detail'),
    path('employee/<int:pk>/attendance', AttendancePageView.as_view(), name='show_attendance_page'),
    path('employee/<int:employee_id>/attendance/give_attendance', giveattendance, name='give_attendance'),
    path('nonsense/', NonsenseView.as_view(), name='display_nonsense'),
    path('nonsense/add', addnonsense, name='add_nonsense'),
    path('restricted', show_restricted_page, name='show_restricted_page'),
    path('restricted/toggle', toggle_access, name='toggle_access')
]