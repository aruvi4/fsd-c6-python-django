from django.db import models

'''
As long as there is a definition for a class here,
Django will automatically create the SQL required for creating the table
It automatically adds an id column unless specifically configured to not add it
It creates other columns based on variable declarations within the class.
AS LONG AS YOUR MODEL CLASS IS A SUBCLASS OF models.Model
FRAMEWORKS HAVE OPINIONATED DEFAULTS
'''

class Employee (models.Model):
    name = models.CharField(max_length = 256)
    salary = models.IntegerField(default = 0)

    def __repr__(self):
        return f'{self.name} draws salary {self.salary}'
    
    def __str__(self):
        return f'{self.name} draws salary {self.salary}'
    
class Attendance (models.Model):
    employee = models.ForeignKey(Employee, on_delete = models.CASCADE)
    date = models.DateTimeField('date')
    is_present = models.BooleanField()

    def __str__(self):
        present = {True: "present", False: "absent"}[self.is_present]
        return f'{self.employee.name} was {present} on {self.date}'

class Nonsense (models.Model):
    message = models.TextField()

    def __str__(self):
        return f'{self.message}'