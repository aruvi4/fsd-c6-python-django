from django.db import models

class Question(models.Model):
    question_text = models.CharField(max_length = 200)
    pub_date = models.DateTimeField('date published', auto_now = True)

    def __str__(self):
        return f'id: {self.id} question: {self.question_text}'

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete = models.CASCADE)
    choice_text = models.CharField(max_length = 50)
    votes = models.IntegerField(default = 0)

    def __str__(self):
        return self.choice_text
