from django.shortcuts import render

from django.http import HttpResponse

def index(request):
    return render(request, 'blog/home.html')

def post_20221101(request):
    return HttpResponse('Today is the 1st of November')