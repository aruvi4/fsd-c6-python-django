from django.urls import path
from .views import index, post_20221101

urlpatterns = [
    path('', index, name = 'index'),
    path('20221101', post_20221101, name = '20221101')
]