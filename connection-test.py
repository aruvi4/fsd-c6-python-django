import mysql.connector

cnx = mysql.connector.connect(user='javauser', password='password',
                              host='127.0.0.1',
                              database='mydb')

cursor = cnx.cursor(named_tuple=True)
print(type(cursor))
query = "SELECT * from EMPLOYEE"

cursor.execute(query)
print(cursor)
for item in cursor:
    print(type(item))
    print(item.name)

cursor.close()
cnx.close()