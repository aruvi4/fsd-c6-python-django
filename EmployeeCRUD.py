import mysql.connector
from Employee import Employee
from sql_exception import ItemNotFoundError

class EmployeeCRUD:

    CREATE = "INSERT into EMPLOYEE values (%s, %s, %s)"
    SEARCH = "SELECT * from EMPLOYEE where id = %s"
    INCREMENT_SALARY = "UPDATE EMPLOYEE SET salary = salary + %(increment)s WHERE id = %(id)s"
    DELETE = "DELETE from EMPLOYEE where id = %s"


    def __init__(self, username, password, hostname, dbname):
        self.cnx = mysql.connector.connect(user = username, password = password, host = hostname, database = dbname)
        
    
    def get_cursor(self):
        return self.cnx.cursor(named_tuple = True)
    
    def create(self, newemployee: Employee) -> bool:
        cursor = self.get_cursor()
        cursor.execute(EmployeeCRUD.CREATE, (newemployee.id, newemployee.name, newemployee.salary))
        self.cnx.commit()
        cursor.close()
        
        return True
    
    def search_by_id(self, empid: int) -> Employee:
        cursor = self.get_cursor()
        cursor.execute(EmployeeCRUD.SEARCH, (empid,))
        search_result = cursor.fetchone()
        cursor.close()
        if search_result == None:
            raise ItemNotFoundError('no employee found with given id')
        return Employee(search_result.id, search_result.name, search_result.salary)
    
    def increment_salary(self, empid: int, increment: int) -> Employee:
        cursor = self.get_cursor()
        named_params = {'increment': increment, 'id': empid}
        cursor.execute(EmployeeCRUD.INCREMENT_SALARY, named_params)
        self.cnx.commit()
        cursor.close()
        return self.search_by_id(empid)
    
    def delete_by_id(self, empid: id) -> bool:
        self.search_by_id(empid)
        cursor = self.get_cursor()
        cursor.execute(EmployeeCRUD.DELETE, (empid,))
        self.cnx.commit()
        cursor.close()
        return True
    
    def close(self) -> None:
        self.cnx.close()
    
    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

with EmployeeCRUD('javauser', 'password', '127.0.0.1', 'mydb') as crud_performer:
    #crud_performer.create(Employee(4, 'Juno', 88))
    #print(crud_performer.increment_salary(4, 10))
    print(crud_performer.delete_by_id(4))
    print(crud_performer.search_by_id(4))
